package com.gurus.orivapp;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends Activity {

    private ImageView imageView;
    private Context c;
    private int number=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = (ImageView)findViewById(R.id.imgPic);
        c = getApplicationContext();

        imageView.setOnTouchListener(new OnSwipeTouchListener(MainActivity.this) {
            public void onSwipeTop() {
                //Toast.makeText(MainActivity.this, "top", Toast.LENGTH_SHORT).show();
            }
            public void onSwipeRight() {
                //izquierda
                //Toast.makeText(MainActivity.this, "right", Toast.LENGTH_SHORT).show();
                switch (number){
                    case 1:
//                        imageView.setBackground(getDrawable(R.drawable.picture1));
                        ImageViewAnimatedChange(c,imageView, BitmapFactory.decodeResource(c.getResources(),
                                R.drawable.picture0));
                        break;
                    case 2:
//                        imageView.setBackground(getDrawable(R.drawable.picture1));
                        ImageViewAnimatedChange(c,imageView, BitmapFactory.decodeResource(c.getResources(),
                                R.drawable.picture1));
                        break;
                    case 3:
//                        imageView.setBackground(getDrawable(R.drawable.picture2));
                        ImageViewAnimatedChange(c,imageView, BitmapFactory.decodeResource(c.getResources(),
                                R.drawable.picture2));
                        break;
                    case 4:
//                        imageView.setBackground(getDrawable(R.drawable.picture3));
                        ImageViewAnimatedChange(c,imageView, BitmapFactory.decodeResource(c.getResources(),
                                R.drawable.picture3));
                        break;
                    case 5:
//                        imageView.setBackground(getDrawable(R.drawable.picture4));
                        ImageViewAnimatedChange(c,imageView, BitmapFactory.decodeResource(c.getResources(),
                                R.drawable.picture4));
                        break;
                    case 6:
//                        imageView.setBackground(getDrawable(R.drawable.picture5));
                        ImageViewAnimatedChange(c,imageView, BitmapFactory.decodeResource(c.getResources(),
                                R.drawable.picture5));
                        break;
                    case 7:
//                        imageView.setBackground(getDrawable(R.drawable.picture5));
                        ImageViewAnimatedChange(c,imageView, BitmapFactory.decodeResource(c.getResources(),
                                R.drawable.picture6));
                        break;
                    default:
                        break;
                }
                number--;
            }
            public void onSwipeLeft() {
                //derecha
                //Toast.makeText(MainActivity.this, "left", Toast.LENGTH_SHORT).show();
                switch (number){
                    case 0:
//                        imageView.setBackground(getDrawable(R.drawable.picture2));
                        ImageViewAnimatedChange(c,imageView, BitmapFactory.decodeResource(c.getResources(),
                                R.drawable.picture1));
                    case 1:
//                        imageView.setBackground(getDrawable(R.drawable.picture2));
                        ImageViewAnimatedChange(c,imageView, BitmapFactory.decodeResource(c.getResources(),
                                R.drawable.picture2));
                        break;
                    case 2:
//                        imageView.setBackground(getDrawable(R.drawable.picture3));
                        ImageViewAnimatedChange(c,imageView, BitmapFactory.decodeResource(c.getResources(),
                                R.drawable.picture3));
                        break;
                    case 3:
//                        imageView.setBackground(getDrawable(R.drawable.picture4));
                        ImageViewAnimatedChange(c,imageView, BitmapFactory.decodeResource(c.getResources(),
                                R.drawable.picture4));
                        break;
                    case 4:
//                        imageView.setBackground(getDrawable(R.drawable.picture5));
                        ImageViewAnimatedChange(c,imageView, BitmapFactory.decodeResource(c.getResources(),
                                R.drawable.picture5));
                        break;
                    case 5:
//                        imageView.setBackground(getDrawable(R.drawable.picture6));
                        ImageViewAnimatedChange(c,imageView, BitmapFactory.decodeResource(c.getResources(),
                                R.drawable.picture6));
                        break;
                    case 6:
//                        imageView.setBackground(getDrawable(R.drawable.picture5));
                        ImageViewAnimatedChange(c,imageView, BitmapFactory.decodeResource(c.getResources(),
                                R.drawable.picture7));
                        break;
                    default:
                        break;
                }
                number++;
            }
            public void onSwipeBottom() {
                //Toast.makeText(MainActivity.this, "bottom", Toast.LENGTH_SHORT).show();
            }

        });
    }

    public static void ImageViewAnimatedChange(Context c, final ImageView v, final Bitmap new_image) {
        final Animation anim_out = AnimationUtils.loadAnimation(c, android.R.anim.fade_out);
        final Animation anim_in  = AnimationUtils.loadAnimation(c, android.R.anim.fade_in);
        anim_out.setAnimationListener(new Animation.AnimationListener()
        {
            @Override public void onAnimationStart(Animation animation) {}
            @Override public void onAnimationRepeat(Animation animation) {}
            @Override public void onAnimationEnd(Animation animation)
            {
                v.setImageBitmap(new_image);
                anim_in.setAnimationListener(new Animation.AnimationListener() {
                    @Override public void onAnimationStart(Animation animation) {}
                    @Override public void onAnimationRepeat(Animation animation) {}
                    @Override public void onAnimationEnd(Animation animation) {}
                });
                v.startAnimation(anim_in);
            }
        });
        v.startAnimation(anim_out);
    }
}
